package com.CompanyCar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CompanyCarApplication {

	public static void main(String[] args) {
		SpringApplication.run(CompanyCarApplication.class, args);
	}

}
