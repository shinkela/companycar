package com.CompanyCar.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.CompanyCar.model.Vehicle;
import com.CompanyCar.service.VehicleService;

@RestController
@RequestMapping("api/vehicles")
public class VehicleController {

	@Autowired
	VehicleService vehicleService;

	@GetMapping("forService")
	public ResponseEntity<List<Vehicle>> vehiclesForService() {

		List<Vehicle> vehiclesForService = vehicleService.findForService();
		return new ResponseEntity<>(vehiclesForService, HttpStatus.OK);
	}

	@PostMapping("/forService/{vehicleId}")
	public ResponseEntity<?> sendForService(@PathVariable Long vehicleId) {

		Vehicle carSent = vehicleService.sendForService(vehicleId);

		return new ResponseEntity<String>(carSent.getName() + " has been sent to service", HttpStatus.OK);
	}

	@GetMapping("/overMileage")
	public ResponseEntity<?> getOverMileage() {
		
		List<Vehicle> vehiclesWithOverMileage = vehicleService.findForOverMile();
		if (vehiclesWithOverMileage.isEmpty()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		System.out.println("vehicle for garbage" + vehiclesWithOverMileage.get(0).getName());
		StringBuilder sb = new StringBuilder();
		for (Vehicle vehicle: vehiclesWithOverMileage) {
			 sb.append(vehicle.getName());
			 sb.append(" ");
			 sb.append("has ran over 300 000 miles. Consider replacing with a new car.\n");
		}
		return new ResponseEntity<String> (sb.toString(), HttpStatus.OK);
	}
}
