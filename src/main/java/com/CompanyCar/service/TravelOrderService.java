package com.CompanyCar.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.CompanyCar.data.TravelOrderRepository;
import com.CompanyCar.model.TravelOrder;
import com.CompanyCar.model.Vehicle;

@Service
public class TravelOrderService {

	@Autowired
	TravelOrderRepository travelOrderRepository;
	@Autowired
	VehicleService vehicleService;

	public TravelOrder create(TravelOrder order) {

		Long vehicleId = order.getVehicle().getId();

		Vehicle chosenVehicle = vehicleService.getOne(vehicleId);
		// adding extra mileage to a vehicle
		chosenVehicle.setMileageToService(chosenVehicle.getMileageToService() + order.getMileage());
		chosenVehicle.setTotalMileage(chosenVehicle.getTotalMileage() + order.getMileage());
		
		vehicleService.checkForService(chosenVehicle);
		vehicleService.findForSuggestNewVehicle(chosenVehicle);
		vehicleService.save(chosenVehicle);
		return travelOrderRepository.save(order);
	}

	public List<TravelOrder> findAll() {
		return travelOrderRepository.findAll();
	}

	
	
}
