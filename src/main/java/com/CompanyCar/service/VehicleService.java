package com.CompanyCar.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.CompanyCar.data.VehicleRepository;
import com.CompanyCar.exceptions.CarIsNotYetReadyForServiceException;
import com.CompanyCar.model.Vehicle;

@Service
public class VehicleService {

	@Autowired
	VehicleRepository vehicleRepository;

	public Vehicle getOne(Long id) {
		return vehicleRepository.getOne(id);
	}

	public Vehicle save(Vehicle vehicle) {
		return vehicleRepository.save(vehicle);
	}

	public List<Vehicle> findForService() {
		return vehicleRepository.findByIsCheckedForServiceTrue();
	}

	public Vehicle sendForService(Long id) {

		Vehicle car = vehicleRepository.getOne(id);
		if (car.isCheckedForService()) {
			car.setCheckedForService(false);
			car.setMileageToService(0);
			vehicleRepository.save(car);
			return car;
		} else {
			throw new CarIsNotYetReadyForServiceException();
		}
	}

	public void findForSuggestNewVehicle(Vehicle chosenVehicle) {
		if (chosenVehicle.getTotalMileage() >= 300000) {
			System.out.println("TOTAL MILEAGE FOR A FCKG CAR" + " " + chosenVehicle.getTotalMileage());
	
			chosenVehicle.setSuggestingForNewVehicle(true);
		}
	}

	public List<Vehicle> findForOverMile() {
		return vehicleRepository.findByIsSuggestingForNewVehicleTrue();
	}

	public void checkForService(Vehicle chosenVehicle) {
		if (chosenVehicle.getMileageToService() >= 50000) {
			chosenVehicle.setCheckedForService(true);
		}
	}

}
