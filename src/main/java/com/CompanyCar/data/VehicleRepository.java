package com.CompanyCar.data;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.CompanyCar.model.Vehicle;

@Repository
public interface VehicleRepository extends JpaRepository<Vehicle, Long>{
	
	public List<Vehicle> findByIsCheckedForServiceTrue();
	public List<Vehicle> findByIsSuggestingForNewVehicleTrue();
}
