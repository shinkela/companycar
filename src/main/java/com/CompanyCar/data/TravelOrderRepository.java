package com.CompanyCar.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.CompanyCar.model.TravelOrder;

@Repository
public interface TravelOrderRepository extends JpaRepository<TravelOrder, Long> {

}
