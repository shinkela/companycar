package com.CompanyCar.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class TravelOrder {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@NotBlank(message = "Employee name cannot be empty.")
	private String employeeName;
	@OneToOne(fetch = FetchType.EAGER)
	private Vehicle vehicle;
	@NotNull(message = "This field is required")
	private double mileage;
	@NotNull(message = "This field is required")
	private double spentFuel;
	@JsonFormat(pattern = "yyyy-mm-dd")
	private Date createdAt;
	@JsonFormat(pattern = "yyyy-mm-dd")
	private Date updatedAt;

	public TravelOrder() {

	}

	public TravelOrder(long id, @NotBlank(message = "Employee name cannot be empty.") String employeeName,
			Vehicle vehicle, @NotNull(message = "This field is required") double mileage,
			@NotNull(message = "This field is required") double spentFuel) {
		super();
		this.id = id;
		this.employeeName = employeeName;
		this.vehicle = vehicle;
		this.mileage = mileage;
		this.spentFuel = spentFuel;
	}

	@PrePersist
	public void onCreate() {
		this.createdAt = new Date();
	}

	@PreUpdate
	public void onUpdate() {
		this.updatedAt = new Date();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getEmployeeName() {
		return employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	public double getMileage() {
		return mileage;
	}

	public void setMileage(double mileage) {
		this.mileage = mileage;
	}

	public double getSpentFuel() {
		return spentFuel;
	}

	public void setSpentFuel(double spentFuel) {
		this.spentFuel = spentFuel;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

}
