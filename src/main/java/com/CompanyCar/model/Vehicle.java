package com.CompanyCar.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PostPersist;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

@Entity
public class Vehicle {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String name;
	private double totalMileage;
	private double mileageToService;
	private boolean isCheckedForService = false;
	private boolean isSuggestingForNewVehicle = false;

//	private static int SEQUENCE = 0;

	public Vehicle() {

	}

	public Vehicle(Long id, String name) {
		super();
		this.id = id;
		this.name = name;

	}

//	@PreUpdate
//	public void countService() {
//		if (this.isCheckedForService == true) {
//			SEQUENCE++;
//		}
//	

	public long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getMileageToService() {
		return mileageToService;
	}

	public void setMileageToService(double mileageToService) {
		this.mileageToService = mileageToService;
	}

	public boolean isCheckedForService() {
		return isCheckedForService;
	}

	public void setCheckedForService(boolean isCheckedForService) {
		this.isCheckedForService = isCheckedForService;
	}

//	public static int getSEQUENCE() {
//		return SEQUENCE;
//	}
//
//	public static void setSEQUENCE(int sEQUENCE) {
//		SEQUENCE = sEQUENCE;
//	}

	public boolean isSuggestingForNewVehicle() {
		return isSuggestingForNewVehicle;
	}

	public void setSuggestingForNewVehicle(boolean isSuggestingForNewVehicle) {
		this.isSuggestingForNewVehicle = isSuggestingForNewVehicle;
	}

	public double getTotalMileage() {
		return totalMileage;
	}

	public void setTotalMileage(double totalMileage) {
		this.totalMileage = totalMileage;
	}
	
	
	
	
}
