package com.CompanyCar.exceptions;

public class CarIsNotYetReadyForServiceException extends RuntimeException {
		
	private static String MESSAGE = "This car has not ran for 50 000 miles for service";
	public CarIsNotYetReadyForServiceException() {
		super(MESSAGE);
	}
}
